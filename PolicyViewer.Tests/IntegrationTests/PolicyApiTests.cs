﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PolicyViewer.Server;
using PolicyViewer.Server.Controllers.Api;
using PolicyViewer.Server.Services.Abstract;
using PolicyViewer.Server.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PolicyViewer.IntegrationTests
{
    public class PolicyApiDefaultTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public PolicyApiDefaultTests()
        {
            var host = new WebHostBuilder()
                .UseStartup<Tests.IntegrationTests.Startup>()
                .UseEnvironment("Test");
            _server = new TestServer(host);
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task Return_Policies()
        {
            var response = await _client.GetAsync("api/policies");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var json = JArray.Parse(responseString);

            Assert.Equal(0, json.Count);
        }

        [Fact]
        public async Task Add_Policies()
        {
            var model = new PolicyVm()
            {
                PolicyNumber = 1234,
                EffectiveDate = new DateTime(2017, 1, 1),
                ExpirationDate = new DateTime(2017, 1, 2),
                PrimaryInsuredName = "Tester",
                RiskConstruction = (byte)RiskConstructionEnum.ModularHome,
                RiskYearBuilt = 1995,
            };

            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("api/policies", content);
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var json = JObject.Parse(responseString);

            Assert.Equal(1, int.Parse(json["policyId"].ToString()) );
        }
    }
}
