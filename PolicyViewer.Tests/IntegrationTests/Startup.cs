﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using PolicyViewer.Server;
using PolicyViewer.Server.Entities;
using PolicyViewer.Server.Repositories;
using PolicyViewer.Server.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using PolicyViewer.Server.Services;
using PolicyViewer.Server.Services.Abstract;

namespace PolicyViewer.Tests.IntegrationTests
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)                
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseInMemoryDatabase();
            });

            services.AddScoped<IEntityRepository<Policy>, EntityBaseRepository<Policy>>();
            services.AddScoped<IEntityRepository<PolicyAddress>, EntityBaseRepository<PolicyAddress>>();
            services.AddScoped<IPolicyService, PolicyService>();

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseExceptionHandler("/Home/Error");

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
