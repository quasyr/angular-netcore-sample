﻿# Policy Viewer

This is a simple web application where a user can create and view insurance policies.

## Build

Requires dotnetcore sdk and nodejs.

In the PolicyViewer folder, build the project.
```
dotnet restore
dotnet build

npm install
npm run build
```

Start the server and browse to the indicated url.
```
dotnet run
```

## Running tests

- Client side tests:  Swtich to the PolicyViewer folder and run `npm run test`

- Server side tests: Swtich to the PolicyViewer.Tests folder and run `dotnet test`

## References

- [Angular4 & DotNetCore](https://github.com/asadsahi/AspNetCoreSpa)