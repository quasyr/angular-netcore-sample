﻿using PolicyViewer.Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Repositories
{
    public class AddressRepository : EntityBaseRepository<PolicyAddress>
    {
        public AddressRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
