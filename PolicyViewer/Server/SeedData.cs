﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PolicyViewer.Server.Entities;
using PolicyViewer.Server.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PolicyViewer.Server
{
    public class SeedData
    {
        readonly ApplicationDbContext _context;

        public SeedData(ApplicationDbContext context)
        {
            _context = context;

            if (_context.RiskConstructions.Any())
            {
                return;   // DB has been seeded
            }

            _context.RiskConstructions.Add(new Entities.RiskConstruction()
            {
                RiskConstructionId = (int)RiskConstructionEnum.BuiltHome,
                Description = "Built Home"
            });
            _context.RiskConstructions.Add(new RiskConstruction()
            {
                RiskConstructionId = (int)RiskConstructionEnum.ModularHome,
                Description = "Modular Home"
            });
            _context.RiskConstructions.Add(new RiskConstruction()
            {
                RiskConstructionId = (int)RiskConstructionEnum.SingleWideManufacturedHome,
                Description = "Single Wide Manufactured Home"
            });
            _context.RiskConstructions.Add(new RiskConstruction()
            {
                RiskConstructionId = (int)RiskConstructionEnum.DoubleWideManufacturedHome,
                Description = "Double Wide Manufactured Home"
            });

            _context.SaveChanges();
        }

    }
}