﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Entities
{
    public class Policy
    {
        [Key]
        public int PolicyId { get; set; }

        [Required]
        public int PolicyNumber { get; set; }
        [Required]
        public DateTime EffectiveDate { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        [StringLength(250)]
        public string PrimaryInsuredName { get; set; }
        public int? RiskYearBuilt { get; set; }

        [Required]
        [ForeignKey("RiskConstruction")]
        public int RiskConstructionId { get; set; }
        public RiskConstruction RiskConstruction { get; set; }

        public ICollection<PolicyAddress> PolicyAddresses { get; set; }

        public Policy()
        {
            PolicyAddresses = new List<PolicyAddress>();
        }

    }

}
