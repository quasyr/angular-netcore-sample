﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Entities
{
    public class RiskConstruction
    {
        [Key]
        public int RiskConstructionId { get; set; }        
        [StringLength(250)]
        public string Description { get; set; }
    }
}
