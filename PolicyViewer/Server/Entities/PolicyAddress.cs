﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Entities
{
    public class PolicyAddress
    {
        [Key]
        public int PolicyAddressId { get; set; }

        public byte AddressType { get; set; }
        
        public int PolicyId { get; set; }
        public Policy Policy { get; set; }

        [StringLength(250)]
        public string Street { get; set; }
        [StringLength(250)]
        public string City { get; set; }
        [StringLength(250)]
        public string State { get; set; }
        [StringLength(250)]
        public string ZipCode { get; set; }
    }

}
