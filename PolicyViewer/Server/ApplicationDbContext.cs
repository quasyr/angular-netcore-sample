﻿using Microsoft.EntityFrameworkCore;
using PolicyViewer.Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Policy> Policies { get; set; }
        public DbSet<PolicyAddress> PolicyAddresses { get; set; }
        public DbSet<RiskConstruction> RiskConstructions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Policy>()
                .HasIndex(f => f.PolicyNumber)
                .IsUnique();

            modelBuilder.Entity<Policy>()
                .Property(f => f.PolicyId)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Policy>()
                .HasMany(f => f.PolicyAddresses)
                .WithOne(f => f.Policy);
        }
    }
}
