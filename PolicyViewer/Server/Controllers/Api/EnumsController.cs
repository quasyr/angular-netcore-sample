using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PolicyViewer.Server.Repositories.Abstract;
using PolicyViewer.Server.Entities;
using PolicyViewer.Server.Services.Abstract;
using NSwag.Annotations;

namespace PolicyViewer.Server.Controllers.Api
{
    [Route("api/enums")]
    public class EnumsController : Controller
    {
        private readonly IEntityRepository<Entities.RiskConstruction> _riskCons;
        public EnumsController(IEntityRepository<Entities.RiskConstruction> riskCons)
        {
            _riskCons = riskCons;
        }

        [SwaggerResponse(typeof(IEnumerable<KeyValuePair<int, string>>))]
        [HttpGet]
        [Route("risk_constructions")]
        public IEnumerable<KeyValuePair<int, string>> GetRiskConstructions()
        {            
            return _riskCons.GetAll()
                .Select(m => new KeyValuePair<int, string>(m.RiskConstructionId, m.Description));
        }
    }
}
