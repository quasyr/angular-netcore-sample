using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PolicyViewer.Server.Repositories.Abstract;
using PolicyViewer.Server.Entities;
using PolicyViewer.Server.Services.Abstract;
using PolicyViewer.Server.ViewModels;
using PolicyViewer.Server.Services;
using NSwag.Annotations;

namespace PolicyViewer.Server.Controllers.Api
{
    [Route("api/policies")]
    public class PolicyController : Controller
    {
        private readonly IEntityRepository<Policy> _policyRepo;
        private readonly IPolicyService _policyService;

        public PolicyController(IEntityRepository<Policy> policyRepo, IPolicyService policyService)
        {
            _policyRepo = policyRepo;
            _policyService = policyService;
        }

        [SwaggerResponse(typeof(IEnumerable<PolicyVm>))]
        [HttpGet]
        public IEnumerable<PolicyVm> GetAllPolicies()
        {
            return _policyRepo.GetAll().Select(m => new PolicyVm()
            {
                PrimaryInsuredName = m.PrimaryInsuredName,
                PolicyNumber = m.PolicyNumber,
                EffectiveDate = m.EffectiveDate,                
                ExpirationDate = m.ExpirationDate
            });
        }

        [HttpPost]
        public IActionResult AddNewPolicy([FromBody] PolicyVm model)
        {
            var policy = _policyService.CreatePolicy(ToEntity(model), 
                model.PrimaryAddress == null ? null : ToEntity(model.PrimaryAddress),
                model.PrimaryAddress == null ? null : ToEntity(model.RiskAddress));

            model.PolicyId = policy.PolicyId;
            return Json(model);
        }
        
        private Policy ToEntity(PolicyVm model)
        {
            var entity = new Policy()
            {
                PolicyNumber = model.PolicyNumber,
                EffectiveDate = model.EffectiveDate,
                ExpirationDate = model.ExpirationDate,
                RiskYearBuilt = model.RiskYearBuilt,
                PrimaryInsuredName = model.PrimaryInsuredName,
                RiskConstructionId = model.RiskConstruction
            };
            return entity;
        }

        private PolicyAddress ToEntity(AddressVm model)
        {
            return new PolicyAddress()
            {
                AddressType = model.AddressType,
                City = model.City,
                State = model.State,
                Street = model.Street,
                ZipCode = model.ZipCode
            };
        }
    }
}
