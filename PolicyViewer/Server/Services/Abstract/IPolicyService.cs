﻿using PolicyViewer.Server.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Services.Abstract
{
    public interface IPolicyService
    {
        Policy CreatePolicy(Policy policy, PolicyAddress primaryAddress, PolicyAddress riskAddress);
    }
}
