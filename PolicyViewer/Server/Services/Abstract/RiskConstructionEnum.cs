﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Services.Abstract
{
    public enum RiskConstructionEnum
    {
        BuiltHome = 1,
        ModularHome,
        SingleWideManufacturedHome,
        DoubleWideManufacturedHome,
    }
}
