﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Services.Abstract
{
    public enum AddressType
    {
        Primary = 1,
        Risk = 2,
    }
}
