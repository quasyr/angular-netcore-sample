﻿using PolicyViewer.Server.Entities;
using PolicyViewer.Server.Repositories.Abstract;
using PolicyViewer.Server.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.Services
{
    public class PolicyService : IPolicyService
    {
        private readonly IEntityRepository<Policy> _policyRepo;
        private readonly IEntityRepository<PolicyAddress> _policyAddressRepo;

        public PolicyService(IEntityRepository<Policy> policyRepo, IEntityRepository<PolicyAddress> policyAddressRepo)
        {
            _policyRepo = policyRepo;
            _policyAddressRepo = policyAddressRepo;
        }

        public Policy CreatePolicy(Policy policy, PolicyAddress primaryAddress, PolicyAddress riskAddress)
        {
            primaryAddress = primaryAddress ?? new PolicyAddress();
            riskAddress = riskAddress ?? new PolicyAddress();

            primaryAddress.AddressType = (byte)AddressType.Primary;
            riskAddress.AddressType = (byte)AddressType.Risk;

            policy.PolicyAddresses.Add(primaryAddress);
            policy.PolicyAddresses.Add(riskAddress);

            foreach (var a in policy.PolicyAddresses)
            {
                a.Policy = policy;
            }

            if (policy.EffectiveDate > policy.ExpirationDate)
                throw new InvalidOperationException("Expiration date must be later than Effective date");

            _policyRepo.Add(policy);
            _policyRepo.Commit();
            return policy;
        }
    }
}
