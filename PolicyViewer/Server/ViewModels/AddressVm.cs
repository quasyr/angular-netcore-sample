﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.ViewModels
{
    public class AddressVm
    {
        public byte AddressType { get; set; }

        public int PolicyId { get; set; }

        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }
}
