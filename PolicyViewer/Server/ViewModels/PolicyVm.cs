﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyViewer.Server.ViewModels
{
    public class PolicyVm
    {
        public int PolicyId { get; set; }

        public int PolicyNumber { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string PrimaryInsuredName { get; set; }
        public byte RiskConstruction { get; set; }
        public int? RiskYearBuilt { get; set; }

        public AddressVm PrimaryAddress { get; set; }
        public AddressVm RiskAddress { get; set; }

    }
}
