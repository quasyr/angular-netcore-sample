﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PolicyViewer.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RiskConstructions",
                columns: table => new
                {
                    RiskConstructionId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Description = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiskConstructions", x => x.RiskConstructionId);
                });

            migrationBuilder.CreateTable(
                name: "Policies",
                columns: table => new
                {
                    PolicyId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EffectiveDate = table.Column<DateTime>(nullable: false),
                    ExpirationDate = table.Column<DateTime>(nullable: false),
                    PolicyNumber = table.Column<int>(nullable: false),
                    PrimaryInsuredName = table.Column<string>(maxLength: 250, nullable: false),
                    RiskConstructionId = table.Column<int>(nullable: false),
                    RiskYearBuilt = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Policies", x => x.PolicyId);
                    table.ForeignKey(
                        name: "FK_Policies_RiskConstructions_RiskConstructionId",
                        column: x => x.RiskConstructionId,
                        principalTable: "RiskConstructions",
                        principalColumn: "RiskConstructionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PolicyAddresses",
                columns: table => new
                {
                    PolicyAddressId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AddressType = table.Column<byte>(nullable: false),
                    City = table.Column<string>(maxLength: 250, nullable: true),
                    PolicyId = table.Column<int>(nullable: false),
                    State = table.Column<string>(maxLength: 250, nullable: true),
                    Street = table.Column<string>(maxLength: 250, nullable: true),
                    ZipCode = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolicyAddresses", x => x.PolicyAddressId);
                    table.ForeignKey(
                        name: "FK_PolicyAddresses_Policies_PolicyId",
                        column: x => x.PolicyId,
                        principalTable: "Policies",
                        principalColumn: "PolicyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Policies_PolicyNumber",
                table: "Policies",
                column: "PolicyNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Policies_RiskConstructionId",
                table: "Policies",
                column: "RiskConstructionId");

            migrationBuilder.CreateIndex(
                name: "IX_PolicyAddresses_PolicyId",
                table: "PolicyAddresses",
                column: "PolicyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PolicyAddresses");

            migrationBuilder.DropTable(
                name: "Policies");

            migrationBuilder.DropTable(
                name: "RiskConstructions");
        }
    }
}
