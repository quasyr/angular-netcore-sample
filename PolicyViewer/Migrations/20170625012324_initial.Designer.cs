﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PolicyViewer.Server;

namespace PolicyViewer.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170625012324_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("PolicyViewer.Server.Entities.Policy", b =>
                {
                    b.Property<int>("PolicyId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("EffectiveDate");

                    b.Property<DateTime>("ExpirationDate");

                    b.Property<int>("PolicyNumber");

                    b.Property<string>("PrimaryInsuredName")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.Property<int>("RiskConstructionId");

                    b.Property<int?>("RiskYearBuilt");

                    b.HasKey("PolicyId");

                    b.HasIndex("PolicyNumber")
                        .IsUnique();

                    b.HasIndex("RiskConstructionId");

                    b.ToTable("Policies");
                });

            modelBuilder.Entity("PolicyViewer.Server.Entities.PolicyAddress", b =>
                {
                    b.Property<int>("PolicyAddressId")
                        .ValueGeneratedOnAdd();

                    b.Property<byte>("AddressType");

                    b.Property<string>("City")
                        .HasMaxLength(250);

                    b.Property<int>("PolicyId");

                    b.Property<string>("State")
                        .HasMaxLength(250);

                    b.Property<string>("Street")
                        .HasMaxLength(250);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(250);

                    b.HasKey("PolicyAddressId");

                    b.HasIndex("PolicyId");

                    b.ToTable("PolicyAddresses");
                });

            modelBuilder.Entity("PolicyViewer.Server.Entities.RiskConstruction", b =>
                {
                    b.Property<int>("RiskConstructionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasMaxLength(250);

                    b.HasKey("RiskConstructionId");

                    b.ToTable("RiskConstructions");
                });

            modelBuilder.Entity("PolicyViewer.Server.Entities.Policy", b =>
                {
                    b.HasOne("PolicyViewer.Server.Entities.RiskConstruction", "RiskConstruction")
                        .WithMany()
                        .HasForeignKey("RiskConstructionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PolicyViewer.Server.Entities.PolicyAddress", b =>
                {
                    b.HasOne("PolicyViewer.Server.Entities.Policy", "Policy")
                        .WithMany("PolicyAddresses")
                        .HasForeignKey("PolicyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
