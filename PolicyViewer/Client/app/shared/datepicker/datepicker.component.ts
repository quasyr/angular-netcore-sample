import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';


import { InputComponent } from '../input/input.component';
@Component({
    selector: 'ui-datepicker',
    templateUrl: './datepicker.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerComponent extends InputComponent implements OnInit {

    
    constructor() {
        super();
        
    }

    ngOnInit() {
    }

    onBlur(event) {
    }
    onChange() {
    }
    onFocus(event) {
    }
    
}