import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InputComponent } from './input/input.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AlertComponent } from './alert/alert.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { FlatpickrModule } from 'angularx-flatpickr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FlatpickrModule.forRoot() 
  ],
  declarations: [
    InputComponent,
    AlertComponent,
    NotFoundComponent,
    DatepickerComponent
  ],  
  exports: [
    InputComponent,
    AlertComponent,
    NotFoundComponent,
    DatepickerComponent
  ]
})
export class SharedModule {}
