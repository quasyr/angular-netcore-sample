import { Component, ChangeDetectionStrategy, Input, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, AbstractControl } from '@angular/forms';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'ui-input',
    templateUrl: './input.component.html'
})
export class InputComponent implements OnChanges  {
    @Input() id: string;
    @Input() label: string;
    @Input() type: string;
    @Input() name: string;
    @Input() placeholder: string = '';
    @Input() errorMessage: string;
    @Input() form: FormGroup;//the parent control
    
    control: AbstractControl
    
    constructor() {
        this.control = new FormControl();
    }

    ngOnChanges() {
        if (this.form)
            this.control = this.form.get(this.name);
    }

    hasErrors = () => this.control && this.control.errors && (this.control.dirty || this.control.touched) 
}
