﻿import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { PolicyListComponent } from './policy-list.component';
import { PolicyService } from './policy.service';
import { SharedModule } from '../shared/shared.module';
import { PolicyVm } from '../_services/api/models';

describe('PolicyComponent', () => {
    let component: PolicyListComponent;
    let fixture: ComponentFixture<PolicyListComponent>;
    let de: DebugElement;
    let el: HTMLElement;

    let service: PolicyService;
    let testPolicy: PolicyVm;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
                imports: [
                    SharedModule
                ],
                declarations: [PolicyListComponent],
                providers: [{ provide: PolicyService, useValue: PolicyService.prototype }]
            })
            .compileComponents();        
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PolicyListComponent);
        
        service = fixture.debugElement.injector.get(PolicyService);
        spyOn(service, 'fetchPolicies')
            .and.returnValue(Promise.resolve());
        spyOn(service, 'fetchRiskConstructions')
            .and.returnValue(Promise.resolve());
          
        component = fixture.componentInstance;
        de = fixture.debugElement.query(By.css('table'));
        el = de.nativeElement;

        testPolicy = {
            policyId: 1,
            policyNumber: 12345,
            effectiveDate: new Date('2017-1-1').toISOString(),
            expirationDate: new Date('2017-1-2').toISOString(),
            primaryInsuredName: 'The Insured',
            riskConstruction: 4,
            riskYearBuilt: 1000,
            primaryAddress: null,
            riskAddress: null,
        };
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should have empty list', () => {        
        var spy = spyOn(service, 'getPolicies')
            .and.returnValue(Observable.of([]));

        fixture.detectChanges();
        expect(el.textContent).toContain('No policies available');
    });

    it('should have one item', () => {
        var spy = spyOn(service, 'getPolicies')
            .and.returnValue(Observable.of([testPolicy]));

        fixture.detectChanges();        
        expect(el.textContent).toContain('The Insured');
        expect(el.textContent).toContain('12345');
    });

});
