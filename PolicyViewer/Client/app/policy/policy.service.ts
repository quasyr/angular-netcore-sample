﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';

import { environment } from '../../environments/environment';
import { ApiClientService } from '../_services/api/index';
import { PolicyVm } from '../_services/api/models';

@Injectable()
export class PolicyService {
    private apiUrl = 'api';  // URL to web api
    private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
    private options = new RequestOptions({ headers: this.headers });

    policies = new BehaviorSubject<any[]>([])
    riskConstructions = new BehaviorSubject<Array<any>>([])
    isLoading = new BehaviorSubject<boolean>(false)

    constructor(private apiClient: ApiClientService) { }

    getPolicies(): Observable<PolicyVm[]> {
        return this.policies.asObservable();
    }

    getRiskConstructions(): Observable<any> {
        return this.riskConstructions.asObservable();
    }

    save(policy: PolicyVm) {
        let r: Promise<any>;
        if (policy.policyId) {
            //r = this.put(policy).toPromise()
            //not implemented
        }
        else {
            r = this.apiClient.Policy_AddNewPolicy(policy).toPromise()
        }
        return r.then(() => {
            this.fetchPolicies()
        });
    }

    fetchRiskConstructions() {        
        return this.apiClient.Enums_GetRiskConstructions()
            .map(r => r.body)
            .toPromise()
            .then(r => {                
                this.riskConstructions.next(r);
            })
    }

    fetchPolicies() {
        this.isLoading.next(true);
        this.apiClient.Policy_GetAllPolicies()
            .map(r => r.body)
            .toPromise()
            .catch(err => {
                this.isLoading.next(false)
                return Promise.reject(err);
            })
            .then(r => {
                this.policies.next(r);
                this.isLoading.next(false);
            })
    }
}
