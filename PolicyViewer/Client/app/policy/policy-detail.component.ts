﻿import { Component, OnInit, OnChanges, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import * as moment from 'moment';

import { PolicyService } from './policy.service';
import { Observable } from 'rxjs';
import { PolicyVm, AddressVm } from '../_services/api/models';

@Component({
    selector: 'app-policy-detail',
    templateUrl: './policy-detail.component.html'
})
export class PolicyDetailComponent implements OnInit, OnChanges, OnDestroy {
    @Input() policy: PolicyVm;
    @Output() saved = new EventEmitter();
    @Output() cancelled = new EventEmitter();

    policyForm: FormGroup;
    riskContstructionValues$: Observable<any>
    
    errorMessage: string;

    constructor(
        private policyService: PolicyService,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router) {
    }

    createForm() {
        return this.fb.group({
            policyNumber: [null, Validators.required],
            primaryInsuredName: ['', Validators.required],
            effectiveDate: [moment().startOf('day').toDate(), Validators.required],
            expirationDate: [moment().startOf('day').toDate(), Validators.required],
            riskConstruction: [1, Validators.required],
            riskYearBuilt: 2000,
            primaryAddress: this.fb.group(this.createAddress()),
            riskAddress: this.fb.group(this.createAddress())
        });
    }
    createAddress() {
        return <AddressVm>{
            policyId: 0,
            city: '',
            state: '',
            street: '',
            zipCode: '',
        }
    }
    formValues(): PolicyVm {
        return Object.assign({}, this.policyForm.value);
    }

    ngOnChanges() {
        this.policyForm.reset(this.policy);
    }

    ngOnInit() {
        this.policyForm = this.createForm();

        this.policyService.fetchRiskConstructions();
        this.riskContstructionValues$ = this.policyService.getRiskConstructions();        

        if (this.router.url === '/policies/create') {

        }
        else {
            this.route.params
                .map((params: Params) => +params['id'])
                .filter(id => id > 0)
                // .switchMap(id => this.policyService.getPolicy(id))
                // .toPromise()
                // .then(v => this.policy = v[0])
                // .catch((err) => console.log('NOT FOUND'))
        }
    }

    ngOnDestroy() {

    }

    onSubmit() {
        let policy = this.formValues();
        this.policyService.save(policy)
                        .then(() => {
                            this.saved.emit();
                            this.gotoList();
                        },
                        err => this.errorMessage = "An error occurred" );
    }

    gotoList() {
        this.router.navigate(['/']);
    }

}
