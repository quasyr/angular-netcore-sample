﻿import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { PolicyService } from './policy.service';
import { PolicyVm } from '../_services/api/models';


@Component({
    selector: 'app-policy',
    templateUrl: './policy-list.component.html'
})
export class PolicyListComponent implements OnInit {
    policies$: Observable<PolicyVm[]>
    isLoading$: Observable<boolean>

    constructor(private policyService: PolicyService) { }

    ngOnInit() {
        this.isLoading$ = this.policyService.isLoading;
        this.policies$ = this.policyService.getPolicies();
        this.policyService.fetchPolicies();
    }

}
