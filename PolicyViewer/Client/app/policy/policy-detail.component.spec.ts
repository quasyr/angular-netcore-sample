﻿import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { PolicyService } from './policy.service';
import { SharedModule } from '../shared/shared.module';
import { PolicyVm } from '../_services/api/models';
import { PolicyDetailComponent } from './policy-detail.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

class PolicyServiceMock extends PolicyService {
    policies = new BehaviorSubject<any[]>([])
    riskConstructions = new BehaviorSubject<Array<any>>([])
    isLoading = new BehaviorSubject<boolean>(false)

    constructor() { 
        super(null)
    }

    getPolicies(): Observable<PolicyVm[]> {
        return this.policies.asObservable();
    }

    getRiskConstructions(): Observable<any> {
        return this.riskConstructions.asObservable();
    }

    save(policy: PolicyVm) {
        return Promise.resolve();
    }

    fetchRiskConstructions() {        
        return Promise.resolve();
    }

    fetchPolicies() {
        return Promise.resolve();
    }
}

describe('PolicyDetailComponent', () => {
    let component: PolicyDetailComponent;
    let fixture: ComponentFixture<PolicyDetailComponent>;

    let service: PolicyServiceMock;
    let testPolicy: PolicyVm;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                SharedModule,
                RouterTestingModule
            ],
            declarations: [PolicyDetailComponent],
            providers: [
                { provide: PolicyService, useClass: PolicyServiceMock },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: Observable.of({})
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PolicyDetailComponent);
        component = fixture.componentInstance;

        service = <PolicyServiceMock>fixture.debugElement.injector.get(PolicyService);

        testPolicy = {
            policyId: 1,
            policyNumber: 12345,
            effectiveDate: new Date('2017-1-1').toISOString(),
            expirationDate: new Date('2017-1-2').toISOString(),
            primaryInsuredName: 'The Insured',
            riskConstruction: 4,
            riskYearBuilt: 1000,
            primaryAddress: null,
            riskAddress: null,
        };

        fixture.detectChanges()
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('saves a policy', fakeAsync(() => {
        let savedSpy = spyOn(component.saved, "emit");

        component.onSubmit();
        tick();
        
        expect(savedSpy).toHaveBeenCalledTimes(1);
    }));

    it('does not save a policy', fakeAsync(() => {
        let savedSpy = spyOn(component.saved, "emit");
        spyOn(service, "save").and.returnValue(Promise.reject(0));

        component.onSubmit();
        tick();
        
        expect(savedSpy).toHaveBeenCalledTimes(0);
    }));

});
