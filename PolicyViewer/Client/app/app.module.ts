﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';;
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PolicyListComponent } from './policy/policy-list.component';
import { PolicyDetailComponent } from './policy/policy-detail.component';
import { PolicyService } from './policy/policy.service';
import { RouterModule } from '@angular/router';
import { routes } from './app.router';
import { NavbarComponent } from './navbar/navbar.component';
import { InputComponent } from './shared/input/input.component';
import { SharedModule } from './shared/shared.module';
import { ApiClientService } from './_services/api';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { FlatpickrModule } from 'angularx-flatpickr';

@NgModule({
    declarations: [
        AppComponent,
        PolicyListComponent,
        PolicyDetailComponent,
        NavbarComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule.forRoot(routes),     
        SharedModule
    ],
    providers: [
        {provide: 'domain', useValue: environment.apiDomain},
        ApiClientService,
        PolicyService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
