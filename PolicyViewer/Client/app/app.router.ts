﻿import { Routes, RouterModule } from '@angular/router';

import { PolicyListComponent } from './policy/policy-list.component';
import { PolicyDetailComponent } from './policy/policy-detail.component';

export const routes: Routes = [
    //{ path: '', redirectTo: 'policies', pathMatch: 'full' },
    { path: '', component: PolicyListComponent },
    { path: 'policies/create', component: PolicyDetailComponent },
    { path: 'policies/:id', component: PolicyDetailComponent },
];