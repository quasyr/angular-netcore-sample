
export interface KeyValuePairOfInt32AndString {
  key: number;
  value: string;
}
