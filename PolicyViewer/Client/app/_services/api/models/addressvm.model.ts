
export interface AddressVm {
  addressType: number;
  policyId: number;
  street: string;
  city: string;
  state: string;
  zipCode: string;
}
