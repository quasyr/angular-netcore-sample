import { AddressVm } from './addressvm.model'

export interface PolicyVm {
  policyId: number;
  policyNumber: number;
  effectiveDate: string;
  expirationDate: string;
  primaryInsuredName: string;
  riskConstruction: number;
  riskYearBuilt: number;
  primaryAddress: AddressVm;
  riskAddress: AddressVm;
}
