import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { KeyValuePairOfInt32AndString, ValueType, PolicyVm, AddressVm } from './models';

/**
* Created with angular4-swagger-client-generator.
*/
@Injectable()
export class ApiClientService {

  private domain = 'http://localhost:5000';

  constructor(private http: HttpClient, @Optional() @Inject('domain') domain: string ) {
    if (domain) {
      this.domain = domain;
    }
  }

  /**
  * Method Home_Index
  * @return The full HTTP response as Observable
  */
  public Home_Index(): Observable<HttpResponse<any>> {
    let uri = `/Home/Index`;
    let headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('post', uri, headers, params, null);
  }

  /**
  * Method Enums_GetRiskConstructions
  * @return The full HTTP response as Observable
  */
  public Enums_GetRiskConstructions(): Observable<HttpResponse<KeyValuePairOfInt32AndString[]>> {
    let uri = `/api/enums/risk_constructions`;
    let headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<KeyValuePairOfInt32AndString[]>('get', uri, headers, params, null);
  }

  /**
  * Method Policy_GetAllPolicies
  * @return The full HTTP response as Observable
  */
  public Policy_GetAllPolicies(): Observable<HttpResponse<PolicyVm[]>> {
    let uri = `/api/policies`;
    let headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<PolicyVm[]>('get', uri, headers, params, null);
  }

  /**
  * Method Policy_AddNewPolicy
  * @param model The 
  * @return The full HTTP response as Observable
  */
  public Policy_AddNewPolicy(model: PolicyVm): Observable<HttpResponse<any>> {
    let uri = `/api/policies`;
    let headers = new HttpHeaders();
    let params = new HttpParams();
    return this.sendRequest<any>('post', uri, headers, params, JSON.stringify(model));
  }

  private sendRequest<T>(method: string, uri: string, headers: HttpHeaders, params: HttpParams, body: any): Observable<HttpResponse<T>> {
    if (method === 'get') {
      return this.http.get<T>(this.domain + uri, { headers: headers.set('Accept', 'application/json'), params: params, observe: 'response' });
    } else if (method === 'put') {
      return this.http.put<T>(this.domain + uri, body, { headers: headers.set('Content-Type', 'application/json'), params: params, observe: 'response' });
    } else if (method === 'post') {
      return this.http.post<T>(this.domain + uri, body, { headers: headers.set('Content-Type', 'application/json'), params: params, observe: 'response' });
    } else if (method === 'delete') {
      return this.http.delete<T>(this.domain + uri, { headers: headers, params: params, observe: 'response' });
    } else {
      console.error('Unsupported request: ' + method);
      return Observable.throw('Unsupported request: ' + method);
    }
  }
}
